__author__ = 'gmoran2017'

codes = {'a': 'm', 'b': 'd', 'c': 'l', 'd': 'x', 'e': '~',
         'f': 't', 'g': 'u', 'h': 'k', 'i': 'z', 'j': '{',
         'k': 'o', 'l': 'y', 'm': 'i', 'n': 'v', 'o': 'p',
         'p': 'q', 'q': 'f', 'r': 'c', 's': 'r', 't': 'b',
         'u': 'j', 'v': 'w', 'w': 'n', 'x': 'h', 'y': 's', 'z': 'a'

    , 'A': 'M', 'B': 'D', 'C': 'L', 'D': 'X', 'E': '`',
         'F': 'T', 'G': 'U', 'H': 'K', 'I': 'Z', 'J': '}',
         'K': 'O', 'L': 'Y', 'M': 'I', 'N': 'V', 'O': 'P',
         'P': 'Q', 'Q': 'F', 'R': 'C', 'S': 'R', 'T': 'B',
         'U': 'J', 'V': 'W', 'W': 'N', 'X': 'H', 'Y': 'S', 'Z': 'A', ' ': 'g', ',': ',', "'": "'", '"': '"', '!': "!",
         '/n': '/n', '\n': '\n', '.': '.', '?': '?', '-': '-', '1': '1', '2': '2', '3': '3'
    , '4': '4', '5': '5', '6': '6', '7': '7', '8': '8', '9': '9', '0': '0', '+': '+', "": "", ":": ":", "(": "(",
         ")": ")"
    , }




# decryption codes creation
codes2 = {}
for key, val in codes.items():
    codes2[val] = key


# encryption function
def encryption(code, s):
    word = ""
    for letter in code:
        word += s[letter] + ' '
    return word


# encryption process
with open("unique.txt", 'r') as file:
    fileText = list(file.read())
print(encryption(fileText, codes))
x = (encryption(fileText, codes))

# write encrypt to file
xFile = open('encrypt.txt', 'w')
xFile.write(x)
xFile.close()

# decryption
xList = x.split(' ')


def decryption(codes2, s):
    word = ""
    for letter in codes2:
        word += s[letter]
    return word


print(decryption(xList, codes2))
y = (decryption(xList, codes2))

# write decrpyt to file
yFile = open('decrypt.txt', 'w')
yFile.write(y)
yFile.close()


# Unique word test
def unique_file():
    input_file = open('unique.txt', 'r')
    file_contents = input_file.read()
    input_file.close()
    word_list = file_contents.split()

    unique = open('Unique_Text.txt', 'w')

    unique_words = set(word_list)
    for word in unique_words:
        unique.write(str(word) + "\n")
        print(word)
    unique.close()


unique_file()
