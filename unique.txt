Before the war, he told me, all the kids went around munching mumpsicles. "Mumpsicles? What is that?" I asked. "What does it sound like," he said with a snort. That's the way it always was with Uncle Hobo. He had seen it all before and who was I to second guess him. I suppose I should be happy that I get to go to his practice for free, ever since I moved in on his street and my parents got stabbed. A kind heart in that one. But every time I go in for a check-up, his remarks make me less than a little bit comfortable.
"Come on in, Gordon. Take a seat. Have a lolly, won't you?" Always with that smile. That's the thing about it- he always respects you first, reels you in with some prospect and then disappoints. Gordon's my name, all right, but I sure as hell dont see no lollies.
And that's not the half of it. He wasnt my uncle anyway but he doesnt like formal titles like "doctor." Says they make him feel old. Threw away his diploma years ago- I sure havent seen it. Hell, he doesnt even wear the garb- calls it pretentious and what have you.
We go through the procedure, same as always: he puts his stethoscope to my bare chest, checks in my eyes and ears, and tells me to say "ah." The only thing that concerns him is my blood pressure. I'm not a big salt eater so this comes as a surprise. "Since when is my blood pressure high, Uncle Hobo? I've been eating my greens, I swear!"
"Have you now, boy?" says Uncle Hobo rifling through some papers, barely looking up. A long pause as he grunts to himself. "Have I ever told you how blood works, Gordon?"
I considered his question. "No, sir, I don't believe you have." I replied, bracing for one of his wordly explanations.
"Well then, to understand why blood is important, we must first turn to veins and arteries."
I listened intently, taking my hand out of my pocket and then putting it back in.
He continued, peering through his spectacles, "Now you see, Gordon, blood is like dogs and veins are like dog racing tracks. Have you ever been to a dog race, Gordon?"
"Yes, sir, I sure have," I exclaimed.
"Well then you of all people should know that veins are like racing tracks for medium-sized dogs, arteries are like racing tracks for large dogs, and capillaries are tracks for small dogs."
I listened intently. I let the knowledge wash over me like awakening to a sunrise.
"In your arteries you got your Alpine Spaniels, your Cesky Terriers, your Lapponian Herders, oh and your Otterhounds..."
I quietly inserted my hand into my pocket, my eyes fixated on his.
"...Gotta remember your Polish Lowland Sheepdogs, your Giant Schnauzers, your Japanese Spitz..."
I began to move my hand back out of my pocket, staring at his face.
"...Your Great Danes in the arteries too, of course. And in your veins, that's where you find your Dachshunds, your Labradors, your Dalmatians, a good deal of Collies..."
I put both of my hands into my pocket and took one of them out, then I put one hand in as I took the other hand out. I was still listening to his fascinating words.
"...your Mastiffs and Boxers. Not to forget your Bloodhounds in your veins and your Bulldogs..."
This time I took both hands out of my pockets and my eyes never left his. I couldnt believe what I was learning about blood.
"In your capillaries--they're the tiniest--you have your Chihuahuas, Pomeranians and Chow Chows, your Poodles, your Bassett Hounds, your Cocker Spaniels, can't forget your English Setters..."
Trying to encourage Uncle Hobo, I shouted at the top of my lungs "WOOF! WOOF! WOOF!" He took it as encouragement and kept talking.
"Also, Gordon, in your capillaries you have your Pugs, your Jack Russell Terriers (remember Wishbone, Gordon?), your Miniature Schnauzers, and your Shitzhus. See what I mean?"
I nodded excitedly and shouted "I sure as hell understand, Uncle Hobo. Better than I ever did before!"
Uncle Hobo looked satisfied. He gazed at me and then reached for something.
"Gordon, it's time for me to give you something." proclaimed Uncle Hobo.
He got a hypodermic needle in his shopping cart. We were on the sidewalk at the time, where he sleeps. He stabbed his shoulder, extracted some blood. Then he stabbed the needle into my shoulder and with a push of his thumb, all his blood entered my bloodstream.
Uncle Hobo explained: "There- now you can see the color 'threeve.'"
Sure enough, there it was all around me. The sky had tinges of 'threeve' and passersby had 'threeve'-colored hair and even there was 'threeve' in a discarded hamburger wrapper on the street. Why couldn't I see 'threeve' before? Was it a special property of Uncle Hobo's own blood? Whatever it was, I was grateful. 'Threeve' would be my new friend.
I thanked Uncle Hobo and left my appointment, but not before giving him a present of my own. I reached into my pocket and pulled out the little man I had hidden there: he stood on my extended palm and performed gymnastic feats before bursting in an explosion of scalding blood. The blood got into Uncle Hobo's eyes and blinded him. I told him it was okay because now he can always see the color 'bars,' because I was a police and he was under arrest. He laughed and he licked my cheek. I handcuffed him- he wasnt a doctor or my uncle. He was a filthy hobo. Case solved.